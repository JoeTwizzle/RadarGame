﻿using UnityEngine;

namespace Assets.Content.Code
{
    internal static class MathHelper
    {
        public static float Angle(Vector2 vector)
        {
            return Mathf.Atan2(vector.y, vector.x);
        }

        public static Vector2 RadianToVector2(float radian)
        {
            return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
        }

        public static Vector2 DegreeToVector2(float degree)
        {
            return RadianToVector2(degree * Mathf.Deg2Rad);
        }

        public static Vector2 RandomPointOnCircleEdge(float radius)
        {
            return Random.insideUnitCircle.normalized * radius;
        }
    }
}
