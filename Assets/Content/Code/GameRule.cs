﻿public abstract class GameRule
{
    public abstract bool Validate(GameContext context, int entityIndex);
}