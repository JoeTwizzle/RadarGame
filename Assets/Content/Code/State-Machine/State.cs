[System.Serializable]
public abstract class State
{
    protected StateMachine _context;
    public State Parent;

    public T GetParent<T>() where T : State
    {
        return Parent as T;
    }

    public State(StateMachine currentContext)
    {
        _context = currentContext;  
    }
    public virtual void ReturnToState() { }

    public virtual void EnterState() { }

    public virtual void UpdateState() { }

    public virtual void ExitState() { }

    public abstract void CheckSwitchStates();

    public void UpdateStates() 
    {
        UpdateState();
        CheckSwitchStates();
    }
}
