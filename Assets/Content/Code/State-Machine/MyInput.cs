using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class MyInput : MonoBehaviour
{
    public static MyInput Instance { get; private set; }
    public StateInput Input;

    public int X, Y, Dx, Dy;
    public bool select, back;



    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        Input = new StateInput();
    }

    private void Update()
    {
        
    }

    void OnEnable()
    {
        Input.StateControls.Enable();
    }

    void OnDisable()
    {
        Input.StateControls.Disable();
    }

    public bool BackPressed()
    {
        return Input.StateControls.Back.WasPressedThisFrame() || back;
    }
    public bool SelectPressed()
    {
        return Input.StateControls.Select.WasPressedThisFrame() || select;
    }

    public float GetDeltaX()
    {
        return Dx;
    }
    public float GetDeltaY()
    {
        return Dy;
    }
}
