using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FlightsState : State
{
    public FlightsState(StateMachine currentContext) : base(currentContext) { }
    int previousX = -1;
    int currentX = -1;
    public override void CheckSwitchStates()
    {
        //TODO: logic
        if (MyInput.Instance.BackPressed())
        {
            _context.PopState();
        }
    }
    public override void UpdateState()
    {
        string txt;
        if (_context.GameContext.PlaneCount > 0)
        {
            currentX += MyInput.Instance.Dx;
            currentX = Mod(currentX, _context.GameContext.PlaneCount);
            if (currentX >= 0 && currentX < _context.GameContext.PlaneCount)
            {
                txt = "< " + _context.GameContext.Planes[currentX].CallSign + " >";
                GameObject.Find("Plane Selector Plane Lable").GetComponent<TextMeshPro>().text = txt;
                previousX = currentX;
                _context.CurrentPlane = _context.GameContext.Planes[currentX];

                GameObject.Find("Plane Selector ALT Value").GetComponent<TextMeshPro>().text = _context.CurrentPlane.Height.ToString("n2")+"ft";
                GameObject.Find("Plane Selector HDG Value").GetComponent<TextMeshPro>().text = _context.CurrentPlane.Heading.ToString("n2")+"�";
                GameObject.Find("Plane Selector SPD Value").GetComponent<TextMeshPro>().text = (_context.CurrentPlane.Speed * 1000).ToString("n2")+"Kts";
            }
        }
    }
    int Mod(int x, int m)
    {
        return (x % m + m) % m;
    }
}