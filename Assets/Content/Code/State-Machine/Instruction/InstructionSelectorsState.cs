using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionSelectorsState : State
{
    public InstructionSelectorsState(StateMachine currentContext) : base(currentContext)
    {
    }

    Renderer p1;
    Renderer p2;
    Renderer p3;
    public override void EnterState()
    {
        p1 = GameObject.Find("Pos On (4)").GetComponent<Renderer>();
        p2 = GameObject.Find("Pos On (1)").GetComponent<Renderer>();
        p3 = GameObject.Find("Pos On (2)").GetComponent<Renderer>();
    }

    public override void CheckSwitchStates()
    {
        if (MyInput.Instance.BackPressed())
        {
            UnsetColor(x);
            _context.PopState();
        }
    }
    int px = -1;
    int x = -1;
    Color pcol = Color.red;
    void UnsetColor(int x)
    {
        if (x == 0)
        {
            p1.material.color = pcol;
        }
        if (x == 1)
        {
            p2.material.color = pcol;
        }
        if (x == 2)
        {
            p3.material.color = pcol;
        }
    }

    void SetColor(int x)
    {
        if (x == 0)
        {
            pcol = p1.material.color;
            p1.material.color = Color.white;
        }
        if (x == 1)
        {
            pcol = p2.material.color;
            p2.material.color = Color.white;
        }
        if (x == 2)
        {
            pcol = p3.material.color;
            p3.material.color = Color.white;
        }
    }

    public override void UpdateState()
    {
        var p = (InstructionState)Parent;
        px = x;
        x = Mathf.Clamp(x - MyInput.Instance.Dx, 0, 2);
        if (px != x)
        {
            UnsetColor(px);
            SetColor(x);
        }
        Debug.Log($"SpeedActive: {p.SpeedActive} AngleActive:{p.AngleActive} AltitudeActive:{p.AltitudeActive}");
        if (x == 0)
        {

            if (MyInput.Instance.SelectPressed())
            {
                p.SpeedActive = !p.SpeedActive;
                pcol = p.SpeedActive ? Color.green : Color.red;
            }
        }
        if (x == 1)
        {
            if (MyInput.Instance.SelectPressed())
            {
                p.AngleActive = !p.AngleActive;
                pcol = p.AngleActive ? Color.green : Color.red;
            }
        }
        if (x == 2)
        {
            if (MyInput.Instance.SelectPressed())
            {
                p.AltitudeActive = !p.AltitudeActive;
                pcol = p.AltitudeActive ? Color.green : Color.red;
            }
        }
    }
}
