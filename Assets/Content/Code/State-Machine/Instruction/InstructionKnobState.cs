﻿using UnityEngine;

internal class InstructionKnobState : State
{
    public InstructionKnobState(StateMachine currentContext) : base(currentContext)
    {
    }

    public override void CheckSwitchStates()
    {
        if (MyInput.Instance.BackPressed())
        {
            _context.PopState();
        }
    }
    GameObject knobGo;

    public override void EnterState()
    {
        knobGo = GameObject.Find("knob");
    }

    public override void ExitState()
    {

    }

    public override void UpdateState()
    {
        if (_context.CurrentPlane != null)
        {
            var p = (InstructionState)Parent;
            p.selected = Mathf.Clamp(p.selected + MyInput.Instance.Dx, 0, 2);
            if (p.selected == 0)
            {
                knobGo.transform.localRotation = Quaternion.Slerp(knobGo.transform.localRotation, Quaternion.Euler(-90, 90, -90), 6 * Time.deltaTime);
            }
            if (p.selected == 1)
            {
                knobGo.transform.localRotation = Quaternion.Slerp(knobGo.transform.localRotation, Quaternion.Euler(-45, 90, -90), 6 * Time.deltaTime);
            }
            if (p.selected == 2)
            {
                knobGo.transform.localRotation = Quaternion.Slerp(knobGo.transform.localRotation, Quaternion.Euler(-0, 90, -90), 6 * Time.deltaTime);
            }
        }
    }
}