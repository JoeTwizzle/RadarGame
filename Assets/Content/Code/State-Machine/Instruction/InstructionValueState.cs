using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InstructionValueState : State
{



    public InstructionValueState(StateMachine currentContext) : base(currentContext)
    {
    }

    public override void EnterState()
    {

    }

    public override void CheckSwitchStates()
    {
        if (MyInput.Instance.BackPressed() || MyInput.Instance.SelectPressed())
        {
            _context.PopState();
        }
    }

    public override void UpdateState()
    {
        var p = (InstructionState)Parent;
        if (_context.CurrentPlane != null)
        {
            if (p.selected == 0)
            {
                p.text.text = p.Speed.ToString("n2") + "KTS";
                p.Speed = Mathf.Clamp(p.Speed + MyInput.Instance.Dy * 10, 10, 2000);
            }
            if (p.selected == 1)
            {
                p.text.text = p.Angle.ToString("n2") + "�";
                p.Angle = Mathf.Clamp(p.Angle + MyInput.Instance.Dy, 0, 360);
            }
            if (p.selected == 2)
            {
                p.text.text = p.Altitude.ToString("n2") + "FT";
                p.Altitude = Mathf.Clamp(p.Altitude + MyInput.Instance.Dy * 25, 5000, 33000);
            }
        }
        else
        {
            p.text.text = "-";
        }
    }
}
