using Assets.Content.Code.Orders;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using TMPro;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;

public class InstructionState : State
{
    public InstructionState(StateMachine currentContext) : base(currentContext)
    {
        targetButton = new TargetButton();
        targetButton.SetUp();
    }
    public TMP_Text text;
    TargetButton targetButton;
    int x, y;
    public float Speed;
    public float Angle;
    public float Altitude;
    public int selected = 0;
    public bool SpeedActive = false;
    public bool AngleActive = false;
    public bool AltitudeActive = false;
    public int activeMode = 0;

    public override void EnterState()
    {
        if (_context.CurrentPlane != null)
        {
            Speed = _context.CurrentPlane.Speed * 1000;
            Angle = _context.CurrentPlane.Heading;
            Altitude = _context.CurrentPlane.Height;
        }
        text = GameObject.Find("POS (1)").GetComponent<TMP_Text>();
        x = y = 0;
        if (_context.CurrentPlane != null)
        {
            Speed = _context.CurrentPlane.Speed;
            Angle = _context.CurrentPlane.Heading;
            Altitude = _context.CurrentPlane.Height;
        }
    }

    public override void UpdateState()
    {
        x -= MyInput.Instance.Dx;
        y -= MyInput.Instance.Dy;
        x = Mathf.Clamp(x, 0, 1);
        y = Mathf.Clamp(y, 0, 2);

        if (_context.CurrentPlane != null)
        {

            if (selected == 0)
            {
                text.text = Speed.ToString("n2") + "KTS";
            }
            if (selected == 1)
            {
                text.text = Angle.ToString("n2") + "�";
            }
            if (selected == 2)
            {
                text.text = Altitude.ToString("n2") + "FT";
            }
        }
        else
        {
            text.text = "-";
        }
    }


    public override void ReturnToState()
    {
        x = y = -1;
        targetButton.Reset();
    }

    public override void CheckSwitchStates()
    {
        targetButton.Choose(x, y);
        if (y == 0)
        {
            if (MyInput.Instance.SelectPressed())
            {
                if (SpeedActive)
                {
                    _context.CurrentPlane.Speed = Speed / 1000f;
                }
                if (AngleActive)
                {
                    _context.CurrentPlane.Heading = Angle;
                }
                if (AltitudeActive)
                {
                    _context.CurrentPlane.Height = Altitude;
                }
                targetButton.Unset(x, y);
                _context.PopState();
            }
        }
        if (x == 0 && y == 1 && MyInput.Instance.SelectPressed())
        {
            targetButton.Unset(x, y);
            _context.PushState(new InstructionKnobState(_context));
        }
        if (x == 1 && y == 1 && MyInput.Instance.SelectPressed())
        {
            targetButton.Unset(x, y);
            _context.PushState(new InstructionValueState(_context));
        }
        if (y == 2 && MyInput.Instance.SelectPressed())
        {
            targetButton.Unset(x, y);
            _context.PushState(new InstructionSelectorsState(_context));
        }
        if (MyInput.Instance.BackPressed())
        {
            targetButton.Unset(x, y);
            _context.PopState();
        }
    }
}
