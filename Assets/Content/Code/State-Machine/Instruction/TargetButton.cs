using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetButton
{
    Renderer k;
    Renderer b;
    Renderer p1;
    Renderer p2;
    Renderer p3;
    Renderer s;

    int px = -1, py = -1;

    public void SetUp()
    {
        k = GameObject.Find("knob").GetComponent<Renderer>();
        b = GameObject.Find("SendButton").GetComponent<Renderer>();
        s = GameObject.Find("Screen").GetComponent<Renderer>();
        p1 = GameObject.Find("Pos On (1)").GetComponent<Renderer>();
        p2 = GameObject.Find("Pos On (2)").GetComponent<Renderer>();
        p3 = GameObject.Find("Pos On (4)").GetComponent<Renderer>();
    }

    Color tempColor;
    Color t1, t2;

    public void Reset()
    {
        px = py = -1;
        t1 = t2 = tempColor = default;
    }

    void Set(int x, int y)
    {
        if (y == 0)
        {
            tempColor = b.material.color;
            b.material.color = Color.white;
        }
        if (y == 1)
        {
            if (x == 0)
            {
                tempColor = k.material.color;
                k.material.color = Color.white;
            }
            if (x == 1)
            {
                tempColor = s.material.color;
                s.material.color = Color.white;
            }
        }
        if (y == 2)
        {
            tempColor = p1.material.color;
            p1.material.color = Color.white;
            t1 = p2.material.color;
            p2.material.color = Color.white;
            t2 = p3.material.color;
            p3.material.color = Color.white;
        }
    }

    public void Unset(int x, int y)
    {
        if (y == 0)
        {
            b.material.color = tempColor;
        }
        if (y == 1)
        {
            if (x == 0)
            {
                k.material.color = tempColor;
            }
            if (x == 1)
            {
                s.material.color = tempColor;
            }
        }
        if (y == 2)
        {
            p1.material.color = tempColor;
            p2.material.color = t1;
            p3.material.color = t2;
        }
    }

    public void Choose(int x, int y)
    {
        Unset(px, py);
        Set(x, y);
        px = x;
        py = y;
    }
}
