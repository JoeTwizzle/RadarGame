using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuleState : State
{
    public RuleState(StateMachine currentContext) : base(currentContext) { }

    public override void CheckSwitchStates()
    {
        if (MyInput.Instance.BackPressed())
        {
            _context.PopState();
        }
    }
}
