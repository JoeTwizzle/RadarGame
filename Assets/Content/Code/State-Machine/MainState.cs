using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct CamStatePair
{
    public Transform Transform;
    public State State;

    public CamStatePair(Transform transform, State state)
    {
        Transform = transform;
        State = state;
    }
}

[System.Serializable]
public class MainState : State
{
    public Renderer renderer;
    public Target target;

    public MainState(StateMachine currentContext) : base(currentContext)
    {
        target = new Target();
        target.SetUp();
    }

    public CamStatePair[] CamStatePairs;

    [SerializeField]
    int selectedPanel;
    public override void EnterState()
    {
        Camera.main.transform.position = GameObject.Find("CamMainPosTarget").transform.position;
        CamStatePairs = new CamStatePair[]
        {
            new CamStatePair(GameObject.Find("SendInfoTarget").transform, new InstructionState(_context)),
            new CamStatePair(GameObject.Find("RadarScreenTarget").transform, new RadarState(_context)),
            new CamStatePair(GameObject.Find("InfoTarget").transform,new FlightsState(_context)),
            new CamStatePair(GameObject.Find("FaxTarget").transform,new RadarState(_context)),
            new CamStatePair(GameObject.Find("RulesTarget").transform,new RadarState(_context))
        };
    }

    public override void CheckSwitchStates()
    {
        if (MyInput.Instance.SelectPressed())
        {
            Camera.main.transform.SetPositionAndRotation(CamStatePairs[selectedPanel].Transform.position, CamStatePairs[selectedPanel].Transform.rotation);
            _context.PushState(CamStatePairs[selectedPanel].State);
            target.Choose(4);
        }
    }
    public override void ReturnToState()
    {
        Camera.main.transform.position = GameObject.Find("CamMainPosTarget").transform.position;
    }
    public override void UpdateState()
    {

        selectedPanel += MyInput.Instance.Dx;
        while (selectedPanel >= CamStatePairs.Length)
        {
            selectedPanel -= CamStatePairs.Length;
        } 
        while (selectedPanel < 0)
        {
            selectedPanel += CamStatePairs.Length;
        }

        Vector3 lookDirection = CamStatePairs[selectedPanel].Transform.position - Camera.main.transform.position;
        lookDirection.Normalize();

        target.Choose(selectedPanel);
        Camera.main.transform.rotation = Quaternion.Slerp(Camera.main.transform.rotation, Quaternion.LookRotation(lookDirection), 6 * Time.deltaTime);
    }
}
