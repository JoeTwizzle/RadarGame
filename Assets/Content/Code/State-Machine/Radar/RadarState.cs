using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarState : State
{
    public RadarState(StateMachine currentContext): base(currentContext) { }

    public override void CheckSwitchStates() 
    {
        if (MyInput.Instance.BackPressed())
        {
            _context.PopState();
        }
    }
}
