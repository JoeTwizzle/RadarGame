using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.LowLevel;

public class StateMachine : MonoBehaviour
{
    State _currentState;
    public GameContext GameContext;
#nullable enable
    public Airplane? CurrentPlane;
#nullable restore
    //get- und set-Methode
    public State CurrentState { get { return _currentState; } }

    Stack<State> _states;

    void Awake()
    {
        _states = new Stack<State>();
        _currentState = new MainState(this);
        _currentState.EnterState();
        GameContext = GetComponent<GameContext>();
        GameContext.OnDespawnPlane += GameContext_OnDespawnPlane;
    }

    private void GameContext_OnDespawnPlane(int obj)
    {
        if (GameContext.Planes[obj] == CurrentPlane)
        {
            CurrentPlane = null;
        }
    }

    void Update()
    {
        _currentState.UpdateStates();
    }

    //public void SetState(State newState)
    //{
    //    _currentState.ExitState();
    //    _currentState = newState;
    //    newState.EnterState();
    //}
    public void PushState(State newState)
    {
        newState.Parent = _currentState;
        _states.Push(_currentState);
        _currentState = newState;
        newState.EnterState();
    }
    public void PopState()
    {
        if (_states.Count > 0)
        {
            var newState = _states.Pop();
            _currentState.ExitState();
            _currentState = newState;
            _currentState.ReturnToState();
        }
    }
    public T GetParentState<T>() where T : State
    {
        return _states.Peek() as T;
    }
}
