using Assets.Content.Code;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[System.Serializable]
public class Airplane
{
    public string CallSign;
    public Vector2 Position;
    public float Heading;
    public float Height;
    public float Speed;

    public static string GetRandomCallsign()
    {
        StringBuilder s = new();
        int length = Random.Range(5, 9);
        for (int i = 0; i < length; i++)
        {
            if (Random.value < 0.75f)
            {
                s.Append((char)Random.Range(65, 91));
            }
            else
            {
                s.Append(Random.Range(0, 10));
            }
        }
        return s.ToString();
    }

    public void UpdatePosition(float dt)
    {
        Position += dt * Speed * MathHelper.DegreeToVector2(Heading);
    }

}
