﻿using Assets.Content.Code.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Content.Code.Events
{
    internal class CreateSetSpeedOrderEvent : GameEvent
    {
        public override void Execute(GameContext context)
        {
            if (context.PlaneCount <= 0)
            {
                return;
            }
            int planeIndex = UnityEngine.Random.Range(0, context.PlaneCount);
            var plane = context.Planes[planeIndex];
            float timeToAct = UnityEngine.Random.Range(7f, 25f);
            float speed = Mathf.Max(10, UnityEngine.Random.Range(plane.Speed - 10, plane.Speed + 30));
            context.GameDay.GameOrders.Add(new SetSpeedOrder(speed, plane, timeToAct));
        }
    }
}
