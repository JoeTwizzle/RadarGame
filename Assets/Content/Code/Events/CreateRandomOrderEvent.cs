﻿using Assets.Content.Code.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Content.Code.Events
{
    internal class CreateRandomOrderEvent : GameEvent
    {
        public override void Execute(GameContext context)
        {
            if (context.PlaneCount <= 0)
            {
                return;
            }
            int planeIndex = UnityEngine.Random.Range(0, context.PlaneCount);
            var plane = context.Planes[planeIndex];
            int possibleOrders = 3;
            int rand = UnityEngine.Random.Range(0, possibleOrders);
            float timeToAct = UnityEngine.Random.Range(7f, 25f);
            if (rand == 0)
            {
                float angle = (int)UnityEngine.Random.Range(plane.Heading - 90f, plane.Heading + 90f);
                context.GameDay.GameOrders.Add(new SetHeadingOrder(angle, plane, timeToAct));
            }
            if (rand == 1)
            {
                float speed = Mathf.Max(10 / 1000f, UnityEngine.Random.Range(plane.Speed - 10/1000f, plane.Speed + 30 / 1000f));
                context.GameDay.GameOrders.Add(new SetSpeedOrder(speed, plane, timeToAct));
            }
            if (rand == 2)
            {
                float height = (int)Mathf.Clamp(UnityEngine.Random.Range(plane.Height - 500, plane.Height + 500), 5000, 33000);
                context.GameDay.GameOrders.Add(new SetHeightOrder(height, plane, timeToAct));
            }
        }
    }
}
