using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlaneEvent : GameEvent
{
    public override void Execute(GameContext context)
    {
        context.SpawnPlane();
    }
}
