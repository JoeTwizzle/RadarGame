﻿using Assets.Content.Code.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Content.Code.Events
{
    internal class CreateSetHeadingOrderEvent : GameEvent
    {
        public override void Execute(GameContext context)
        {
            if (context.PlaneCount <= 0)
            {
                return;
            }
            int planeIndex = UnityEngine.Random.Range(0, context.PlaneCount);
            var plane = context.Planes[planeIndex];
            float timeToAct = UnityEngine.Random.Range(7f, 25f);
            float angle = UnityEngine.Random.Range(plane.Heading - 90f, plane.Heading + 90f);
            context.GameDay.GameOrders.Add(new SetHeadingOrder(angle, plane, timeToAct));
        }
    }
}
