﻿using Assets.Content.Code.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Content.Code.Events
{
    internal class CreateSetAltitudeOrderEvent : GameEvent
    {
        public override void Execute(GameContext context)
        {
            if (context.PlaneCount <= 0)
            {
                return;
            }
            int planeIndex = UnityEngine.Random.Range(0, context.PlaneCount);
            var plane = context.Planes[planeIndex];
            float timeToAct = UnityEngine.Random.Range(7f, 25f);
            float height = Mathf.Clamp(UnityEngine.Random.Range(plane.Height - 5000, plane.Height + 33000), 5000, 33000);
            context.GameDay.GameOrders.Add(new SetHeightOrder(height, plane, timeToAct));
        }
    }
}
