using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDay
{
    public List<GameRule> GameRules;
    public List<GameEvent> GameEvents;
    public List<GameOrder> GameOrders;
    public List<Airplane> AllPlanes;
    public GameDay()
    {
        AllPlanes = new();
        GameRules = new List<GameRule>();
        GameEvents = new List<GameEvent>();
        GameOrders = new List<GameOrder>();
    }

    public void Update(GameContext context)
    {
        for (int i = GameEvents.Count - 1; i >= 0; i--)
        {
            if (GameEvents[i].TimeOfOccurance <= 0)
            {
                Debug.Log($"Executing event at: {i} it is a {GameEvents[i].GetType().Name} event");
                GameEvents[i].Execute(context);
                Debug.Log($"Removing event at: {i} it was a {GameEvents[i].GetType().Name} event");
                GameEvents.RemoveAt(i);
                i--;
                continue;
            }
            GameEvents[i].TimeOfOccurance -= context.DeltaTime;
        }
        for (int i = 0; i < GameRules.Count; i++)
        {
            for (int planes = 0; planes < context.PlaneCount; planes++)
            {
                GameRules[i].Validate(context, planes);
            }
        }
        for (int i = 0; i < GameOrders.Count; i++)
        {
            GameOrders[i].Update(context);
        }
    }
}
