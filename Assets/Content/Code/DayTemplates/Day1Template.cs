﻿using Assets.Content.Code.Events;
using Assets.Content.Code.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Content.Code.DayTemplates
{
    internal class Day1Template
    {
        public static GameDay Create()
        {
            var day = new GameDay();
            day.GameRules.Add(new AvoidOthersRule());
            day.GameEvents.Add(new SpawnPlaneEvent() { TimeOfOccurance = 1f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 3f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 14f });
            day.GameEvents.Add(new SpawnPlaneEvent() { TimeOfOccurance = 20f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 23f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 42f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 45f });
            day.GameEvents.Add(new SpawnPlaneEvent() { TimeOfOccurance = 55f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 56f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 62f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 65f });
            day.GameEvents.Add(new SpawnPlaneEvent() { TimeOfOccurance = 82f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 86f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 92f });
            day.GameEvents.Add(new CreateRandomOrderEvent() { TimeOfOccurance = 95f });
            return day;
        }
    }
}
