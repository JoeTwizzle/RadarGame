using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class SerialDataReciever : MonoBehaviour
{

    public TMP_Dropdown dropdown;
    public Button button;
    private Queue<byte> recievedData = new Queue<byte>();

    // Start is called before the first frame update
    void Start()
    {
        var ports = SerialPort.GetPortNames();
        dropdown.options.Clear();
        for (int i = 0; i < ports.Length; i++)
        {
            dropdown.options.Add(new TMP_Dropdown.OptionData(ports[i]));
        }
    }
    SerialPort port = null;
    float pmx, pmy, mx, my;
    // Update is called once per frame
    void Update()
    {
        MyInput.Instance.select = false;
        MyInput.Instance.back = false;
        MyInput.Instance.Dx = MyInput.Instance.Dy = 0;
        if (port != null && port.IsOpen)
        {
            while (port.BytesToRead >= 4)
            {
                sbyte dx = (sbyte)port.ReadByte();
                sbyte dy = (sbyte)port.ReadByte();
                bool backPressed = port.ReadByte() != 0;
                bool selectPressed = port.ReadByte() != 0;
                Debug.Log($"Dx:{dx},Dy:{dy},Select:{selectPressed},Back:{backPressed}");
                MyInput.Instance.X += dx;
                MyInput.Instance.Y += dy;
                MyInput.Instance.select |= selectPressed;
                MyInput.Instance.back |= backPressed;
                MyInput.Instance.Dx += dx;
                MyInput.Instance.Dy += dy;
            }
        }
        else
        {
            pmx = mx;
            pmy = my;
            mx += MyInput.Instance.Input.StateControls.X.ReadValue<float>() / 250f;
            my += MyInput.Instance.Input.StateControls.Y.ReadValue<float>() / 250f;

            MyInput.Instance.X = (int)(mx);
            MyInput.Instance.Y = (int)(my);
            MyInput.Instance.Dx = -((int)mx - (int)pmx);
            MyInput.Instance.Dy = ((int)my - (int)pmy);
            //Debug.Log($"Mx:{mx},My:{my},Pmx:{pmx},Pmy:{pmy},Dx:{(int)(mx - pmx)},Dy:{(int)(my - pmy)}");
        }
    }

    public void OnValueChanged(int index)
    {

    }

    public void OnConnectClicked()
    {
        if (port != null)
        {
            port.Dispose();
        }
        try
        {
            port = new SerialPort(dropdown.options[dropdown.value].text, 115200);
            port.Open();
            port.DataReceived += Port_DataReceived;
            Debug.Log("Connected to Serial port: " + dropdown.options[dropdown.value].text);
        }
        catch
        {
            Debug.Log("The serial port could not be opened");
        }
    }

    private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
    {
        byte[] data = new byte[port.BytesToRead];
        port.Read(data, 0, data.Length);
        for (int i = 0; i < data.Length; i++)
        {
            recievedData.Enqueue(data[i]);
        }
        Debug.Log("Recieved " + port.BytesToRead + " bytes!");
    }
}
