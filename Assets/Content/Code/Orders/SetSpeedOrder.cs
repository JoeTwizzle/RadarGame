﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Content.Code.Orders
{
    internal class SetSpeedOrder : GameOrder
    {
        public float DesiredSpeed;

        public SetSpeedOrder(float desiredSpeed, Airplane airplane, float timeToAct) : base(airplane, timeToAct)
        {
            DesiredSpeed = desiredSpeed;
            Description = GetRandomDescription();
        }

        protected override CompletionState Check(GameContext context, Airplane plane)
        {
            return Mathf.Abs(plane.Speed - DesiredSpeed) > 100/1000f ? CompletionState.None : CompletionState.Completed;
        }

        string GetRandomDescription()
        {
            var speed = Airplane.Speed;
            var diff = DesiredSpeed - speed;
            var rnd = UnityEngine.Random.Range(0, 3);
            StringBuilder sb = new StringBuilder();
            if (rnd != 0)
            {
                if (diff > 0)
                {
                    sb.Append("Increase ");
                }
                else
                {
                    sb.Append("Decrease ");
                }
                sb.Append("speed of ");
                sb.Append(Airplane.CallSign);
                sb.Append(" by ");
                sb.Append(diff * 1000);
                sb.Append(" knots");
            }
            else
            {
                sb.Append($"Set speed of {Airplane.CallSign} to {DesiredSpeed * 1000:n2} knots");
            }
            return sb.ToString();
        }
    }
}
