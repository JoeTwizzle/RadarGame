﻿
using System.Text;
using UnityEngine;

namespace Assets.Content.Code.Orders
{
    internal class SetHeadingOrder : GameOrder
    {
        public float DesiredHeading;

        public SetHeadingOrder(float desiredHeading, Airplane airplane, float timeToAct) : base(airplane, timeToAct)
        {
            DesiredHeading = desiredHeading;
            Description = GetRandomDescription();
        }

        protected override CompletionState Check(GameContext context, Airplane plane)
        {
            return Mathf.Abs(plane.Heading - DesiredHeading) > 5f ? CompletionState.None : CompletionState.Completed;
        }

        string GetRandomDescription()
        {
            var heading = Airplane.Heading;
            var diff = DesiredHeading - heading;
            var rnd = UnityEngine.Random.Range(0, 3);
            StringBuilder sb = new StringBuilder();
            if (rnd != 0)
            {
                if (diff > 0)
                {
                    sb.Append("Turn right ");
                }
                else
                {
                    sb.Append("Turn left ");
                }
                sb.Append("heading ");
                sb.Append(DesiredHeading.ToString("n2"));
                sb.Append("° flight ");
                sb.Append(Airplane.CallSign);
            }
            else
            {
                rnd = UnityEngine.Random.Range(0, 3);
                if (rnd == 0)
                {
                    sb.Append($"Advise flight {Airplane.CallSign} face heading ");
                    if (diff > 0)
                    {
                        sb.Append("right by ");
                    }
                    else
                    {
                        sb.Append("left by ");
                    }
                    sb.Append($"to {diff:n2}°");
                }
                else
                {
                    sb.Append($"{Airplane.CallSign} {diff:n2}° ");
                }
            }
            return sb.ToString();
        }
    }
}
