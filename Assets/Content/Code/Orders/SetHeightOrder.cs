﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Content.Code.Orders
{
    internal class SetHeightOrder : GameOrder
    {
        public float DesiredHeight;

        public SetHeightOrder(float desiredHeight, Airplane airplane, float timeToAct) : base(airplane, timeToAct)
        {
            DesiredHeight = desiredHeight;
            Description = GetRandomDescription();
        }

        protected override CompletionState Check(GameContext context, Airplane plane)
        {
            return Mathf.Abs(plane.Height - DesiredHeight) > 100f ? CompletionState.None : CompletionState.Completed;
        }
        string GetRandomDescription()
        {
            var height = Airplane.Height;
            var diff = DesiredHeight - height;
            var rnd = UnityEngine.Random.Range(0, 3);
            StringBuilder sb = new StringBuilder();
            if (rnd != 0)
            {
                if (diff > 0)
                {
                    sb.Append("Increase ");
                }
                else
                {
                    sb.Append("Decrease ");
                }
                sb.Append("altitude of ");
                sb.Append(Airplane.CallSign);
                sb.Append(" by ");
                sb.Append(diff);
                sb.Append(" feet");
            }
            else
            {
                rnd = UnityEngine.Random.Range(0, 3);
                if (rnd == 0)
                {
                    sb.Append($"Advise {Airplane.CallSign} ");
                    if (diff > 0)
                    {
                        sb.Append("climb ");
                    }
                    else
                    {
                        sb.Append("descend ");
                    }
                    sb.Append($"to {DesiredHeight:n2} feet");
                }
                else
                {
                    sb.Append($"Advise {Airplane.CallSign} set altitude ");
                    sb.Append($"to {DesiredHeight:n2} feet");
                }
            }
            return sb.ToString();
        }
    }
}
