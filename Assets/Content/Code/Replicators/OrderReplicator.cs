using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class OrderReplicator : MonoBehaviour
{
    public TMP_Text faxText;
    GameContext context;
    // Start is called before the first frame update
    void Start()
    {
        context = GetComponent<GameContext>();

    }

    // Update is called once per frame
    void Update()
    {
        faxText.text = "";
        for (int i = 0; i < context.GameDay.GameOrders.Count; i++)
        {
            var order = context.GameDay.GameOrders[i];
            if (order.State == GameOrder.CompletionState.None && order.Description != null)
            {
                faxText.text += order.Description + "\n";
            }
        }
    }
}
