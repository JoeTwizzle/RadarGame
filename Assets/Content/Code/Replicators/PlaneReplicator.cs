using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class PlaneReplicator : MonoBehaviour
{
    public Transform UIRoot;
    public GameObject planeUIPrefab;
    GameContext context;
    ObjectPool<GameObject> pools;
    Dictionary<int, GameObject> activePlanes;
    // Start is called before the first frame update
    void Start()
    {
        activePlanes = new Dictionary<int, GameObject>();
        pools = new ObjectPool<GameObject>(CreatePlane, GetPlane, ReleasePlane);
        context = GetComponent<GameContext>();
        context.OnSpawnPlane += Context_OnSpawnPlane;
        context.OnDespawnPlane += Context_OnDespawnPlane;
    }

    GameObject CreatePlane()
    {
        var plane = Instantiate(planeUIPrefab, UIRoot);
        plane.SetActive(false);
        return plane;
    }

    void GetPlane(GameObject plane)
    {
        plane.SetActive(true);
    }

    void ReleasePlane(GameObject plane)
    {
        plane.SetActive(false);
    }

    private void Context_OnSpawnPlane(int id)
    {
        var plane = pools.Get();
        activePlanes.Add(id, plane);
    }

    private void Context_OnDespawnPlane(int id)
    {
        pools.Release(activePlanes[id]);
        activePlanes.Remove(id);
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var plane in activePlanes)
        {
            var pos = context.Planes[plane.Key].Position;
            plane.Value.transform.position = new Vector3(pos.x, pos.y, 0);
        }
    }
}
