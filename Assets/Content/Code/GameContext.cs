using Assets.Content.Code;
using Assets.Content.Code.DayTemplates;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameContext : MonoBehaviour
{
    public event Action<int> OnSpawnPlane;
    public event Action<int> OnDespawnPlane;

    public Airplane[] Planes;
    public int PlaneCount;
    public GameDay GameDay;
    public float DeltaTime;

    // Start is called before the first frame update
    void Start()
    {
        GameDay = Day1Template.Create();
        Planes = new Airplane[512]; //maximum number of planes
    }

    // Update is called once per frame
    void Update()
    {
        DeltaTime = Time.deltaTime;
        GameDay.Update(this);
        for (int i = 0; i < PlaneCount; i++)
        {
            Planes[i].UpdatePosition(Time.deltaTime);
        }
    }

    public void SpawnPlane()
    {
        var plane = Planes[PlaneCount++] = new();
        GameDay.AllPlanes.Add(plane);
        plane.Position = MathHelper.RandomPointOnCircleEdge(1f);
        Debug.Log($"P1: {plane.Position}");
        var p2 = MathHelper.RandomPointOnCircleEdge(1f);
        while (Vector2.Distance(plane.Position, p2) < 0.7f)
        {
            p2 = MathHelper.RandomPointOnCircleEdge(1f);
        }
        Debug.Log($"P2: {p2}");
        float angle = (Mathf.Rad2Deg * MathHelper.Angle((p2 - plane.Position).normalized));
        if (angle < 0)
        {
            angle += 360;
        }
        plane.Heading = angle % 360f;
        Debug.Log($"Angle: {plane.Heading}");
        plane.Height = UnityEngine.Random.Range(5000, 33000);
        plane.Speed = Mathf.Lerp(0.025f, 0.05f, Mathf.InverseLerp(5000, 33000, plane.Height)) * Mathf.Lerp(0.7f, 1f, UnityEngine.Random.value);
        plane.CallSign = Airplane.GetRandomCallsign();
        OnSpawnPlane?.Invoke(PlaneCount - 1);
    }


    public void DespawnPlane(int index)
    {
        Array.Copy(Planes, index, Planes, index - 1, PlaneCount - (index + 1));
        PlaneCount--;
        OnDespawnPlane?.Invoke(index);
    }
}
