﻿using System;

public abstract class GameEvent : IComparable<GameEvent>
{
    public float TimeOfOccurance;

    public int CompareTo(GameEvent other)
    {
        return TimeOfOccurance.CompareTo(other.TimeOfOccurance);
    }

    public abstract void Execute(GameContext context);
}