﻿/// <summary>
/// The order the player needs to execute
/// </summary>
public abstract class GameOrder
{
    public enum CompletionState
    {
        None,
        Completed,
        Failed
    }
    public Airplane Airplane;
    public float TimeToAct;
    public string Description;
    protected GameOrder(Airplane airplane, float timeToAct)
    {
        Airplane = airplane;
        TimeToAct = timeToAct;
    }

    public CompletionState State { get; protected set; } = CompletionState.None;
    protected abstract CompletionState Check(GameContext context, Airplane plane);
    public void Update(GameContext context)
    {
        if (State == CompletionState.None)
        {
            if (State != CompletionState.Failed)
            {
                State = Check(context, Airplane);
            }
            TimeToAct -= context.DeltaTime;
            if (TimeToAct <= 0)
            {
                State = CompletionState.Failed;
            }
        }
    }
}