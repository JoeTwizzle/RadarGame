using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidAreaRule : GameRule
{
    Vector2 Min;
    Vector2 Max;
    public AvoidAreaRule(Vector2 p1, Vector2 p2)
    {
        Min = Vector2.Min(p1, p2);
        Max = Vector2.Max(p1, p2);
    }

    bool IsInside(Vector2 pos)
    {
        return Min.x < pos.x && Max.x > pos.x && Min.y < pos.y && Max.y > pos.y;
    }

    public override bool Validate(GameContext context, int entityIndex)
    {
        return !IsInside(context.Planes[entityIndex].Position);
    }
}
