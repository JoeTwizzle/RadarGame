﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidOthersRule : GameRule
{
    public override bool Validate(GameContext context, int planeIndex)
    {
        for (int i = 0; i < context.PlaneCount; i++)
        {
            if (i != planeIndex)
            {
                if (Vector2.Distance(context.Planes[planeIndex].Position, context.Planes[i].Position) < 0.1f)
                {
                    return false;
                }
            }
        }
        return true;
    }
}
