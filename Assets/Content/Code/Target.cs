using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target
{
    Renderer ic;
    Renderer op;
    Renderer op1;
    Renderer pi;
    Renderer fm;
    Renderer wb;

    public void SetUp()
    {
        ic = GameObject.Find("Info Cube").GetComponent<Renderer>();
        op = GameObject.Find("oldPC").GetComponent<Renderer>();
        op1 = GameObject.Find("PC").GetComponent<Renderer>();
        pi = GameObject.Find("Plane info").GetComponent<Renderer>();
        fm = GameObject.Find("Fax machine").GetComponent<Renderer>();
        wb = GameObject.Find("WhiteboardDrawingsurface").GetComponent<Renderer>();
    }

    public void Choose(int c)
    {
        if (c == 2)
        {
            wb.material.color = Color.gray;
            ic.material.color = Color.white;
            op.material.color = Color.gray;
            op1.material.color = Color.gray;
            pi.material.color = Color.gray;
            fm.material.color = Color.gray;
        }
        if (c == 1)
        {
            wb.material.color = Color.gray;
            ic.material.color = Color.gray;
            op.material.color = Color.white;
            op1.material.color = Color.white;
            pi.material.color = Color.gray;
            fm.material.color = Color.gray;
        }
        if (c == 0)
        {
            wb.material.color = Color.gray;
            ic.material.color = Color.gray;
            op.material.color = Color.gray;
            op1.material.color = Color.gray;
            pi.material.color = Color.white;
            fm.material.color = Color.gray;
        }
        if (c == 3)
        {
            wb.material.color = Color.gray;
            ic.material.color = Color.gray;
            op.material.color = Color.gray;
            op1.material.color = Color.gray;
            pi.material.color = Color.gray;
            fm.material.color = Color.white;
        }
        if (c == 4)
        {
            wb.material.color = Color.gray;
            ic.material.color = Color.gray;
            op.material.color = Color.gray;
            op1.material.color = Color.gray;
            pi.material.color = Color.gray;
            fm.material.color = Color.gray;
        }
        if (c == 5)
        {
            wb.material.color = Color.white;
            ic.material.color = Color.gray;
            op.material.color = Color.gray;
            op1.material.color = Color.gray;
            pi.material.color = Color.gray;
            fm.material.color = Color.gray;
        }
    }
}

